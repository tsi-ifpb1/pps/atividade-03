package fm;

public class Cliente {
	
	public static void main(String[] args) {
		FactoryUsuario fabrica = new FactoryUsuario();
		Usuario user = fabrica.getUsuario("Mauricio", "masculino");
		System.out.println(user.saudacao());
	}

}
