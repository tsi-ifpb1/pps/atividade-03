package fm;

public class Men extends Usuario{
	
	public Men(String nome, String genero) {
		super(nome, genero);
		
	}
	
	public String saudacao() {
		return "Bem-vindo Sr. " + super.getNome();
	}

}
