package fm;

public class FactoryUsuario {
	
	public FactoryUsuario() {}
	
	public Usuario getUsuario(String nome, String genero) {
		if(genero.equals("masculino")) {
			return new Men(nome, genero);
		} else if (genero.equals("feminino")) {
			return new Women(nome, genero);
		} else if (genero.equals("ausente")){
			return new Undefined(nome, genero);
		} else {
			return null;
		}
	}

}
